package todolint

import (
	"os"
	"testing"
)

func TestReadDir(t *testing.T) {
	var err error

	dirname := "rumpelstilzchen"
	_, err = ReadDir(dirname)
	if err == nil {
		t.Fatalf("ReadDir %s: error expected, none found", dirname)
	}

	dirname = ".."
	var list []os.FileInfo
	list, err = ReadDir(dirname)
	if err != nil {
		t.Fatalf("ReadDir %s: %v", dirname, err)
	}

	var foundFile = false
	var foundSubDir = false
	for _, dir := range list {
		switch {
		case !dir.IsDir() && dir.Name() == "../main.go":
			foundFile = true
		case dir.IsDir() && dir.Name() == "../lib":
			foundSubDir = true
		}
	}
	if !foundFile {
		t.Fatalf("ReadDir %s: ../main.go file not found", dirname)
	}
	if !foundSubDir {
		t.Fatalf("ReadDir %s: ../lib directory not found", dirname)
	}
}
