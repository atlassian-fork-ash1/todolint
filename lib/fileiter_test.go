package todolint

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

func TestIteratorInit(t *testing.T) {
	var err error
	var dirname string

	dirname = "rumpelstilzchen"
	_, err = NewFileIter(dirname)
	if err == nil {
		t.Fatalf("NewFileIter %s: error expected, none found", dirname)
	}

	dirname = ".."
	var iterator *FileIterator
	iterator, err = NewFileIter(dirname)
	if err != nil {
		t.Fatalf("NewFileIter %s: %v", dirname, err)
	}

	var dir os.FileInfo
	var foundFile = false
	var foundSubFile = false
	for dir, err = iterator.Next(); err == nil; dir, err = iterator.Next() {
		switch {
		case !dir.IsDir() && dir.Name() == "../main.go":
			foundFile = true
		case !dir.IsDir() && dir.Name() == "../lib/fileiter_test.go":
			foundSubFile = true
		}
	}
	switch err.(type) {
	case IteratorEmptyError:
		break
	case nil:
		break
	default:
		t.Fatalf("FileIterator.Next %s: %v", dirname, err)
	}
	if !foundFile {
		t.Fatalf("FileIterator.Next %s: ../main.go file not found", dirname)
	}
	if !foundSubFile {
		t.Fatalf("FileIterator.Next %s: ../lib/fileiter_test.go file not found", dirname)
	}
}

func TestIteratorEmptyDir(t *testing.T) {
	var err error
	var dirname string

	dirname, err = ioutil.TempDir("", "fileiter_test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dirname)

	var iterator *FileIterator
	iterator, err = NewFileIter(dirname)
	if err != nil {
		t.Fatalf("NewFileIter %s: %v", dirname, err)
	}

	var foundFile = false
	for _, err = iterator.Next(); err == nil; _, err = iterator.Next() {
		foundFile = true
	}
	switch err.(type) {
	case IteratorEmptyError:
		break
	default:
		t.Fatalf("FileIterator.Next %s: %v", dirname, err)
	}
	if foundFile {
		t.Fatalf("FileIterator.Next %s: Found a file but expected none.", dirname)
	}
}

func TestIteratorEmptysubDir(t *testing.T) {
	var err error
	var dirname string

	dirname, err = ioutil.TempDir("", "filiter_test")
	if err != nil {
		t.Fatal(err)
	}
	err = os.Mkdir(filepath.Join(dirname, "emptySubDir"), os.ModePerm)
	if err != nil {
		t.Fatal(err)
	}
	err = os.Mkdir(filepath.Join(dirname, "emptySubDir", "emptySubSubDir"), os.ModePerm)
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(filepath.Join(dirname, "emptySubDir", "emptySubSubDir"))
	defer os.RemoveAll(filepath.Join(dirname, "emptySubDir"))
	defer os.RemoveAll(dirname)

	var iterator *FileIterator
	iterator, err = NewFileIter(dirname)
	if err != nil {
		t.Fatalf("NewFileIter %s: %v", dirname, err)
	}

	for _, err = iterator.Next(); err == nil; _, err = iterator.Next() {
	}
	switch err.(type) {
	case IteratorEmptyError:
		break
	default:
		t.Fatalf("FileIterator.Next %s: %v", dirname, err)
	}
}
