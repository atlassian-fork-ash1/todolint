package todolint

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"testing"
)

func TestLineIteratorInitButEmpty(t *testing.T) {
	var err error
	var dirname string

	dirname, err = ioutil.TempDir("", "lineiter_test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dirname)

	var fileIter *FileIterator
	fileIter, err = NewFileIter(dirname)
	if err != nil {
		t.Fatalf("LineIter: Expected FileIter to create without error. Got: %s", err)
	}

	_, err = NewMultiFileLineIterator(fileIter)
	switch err.(type) {
	case IteratorEmptyError:
		break
	default:
		t.Fatalf("LineIter: Expected IteratorEmptyError but got: %s", err)
	}
}

func TestLineIteratorInit(t *testing.T) {
	var err error
	var dirname string

	dirname, err = ioutil.TempDir("", "lineiter_test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dirname)

	err = ioutil.WriteFile(filepath.Join(dirname, "main.go"), []byte("words"), os.ModePerm)
	if err != nil {
		t.Fatalf("LineIter: Tried to create test file but got: %s", err)
	}

	var fileIter *FileIterator
	fileIter, err = NewFileIter(dirname)
	if err != nil {
		t.Fatalf("LineIter: Expected FileIter to create without error. Got: %s", err)
	}

	_, err = NewMultiFileLineIterator(fileIter)
	if err != nil {
		t.Fatalf("LineIter: Expected nil but got: %s", err)
	}

}

func TestLineIteratorSingleLine(t *testing.T) {
	var err error
	var dirname string

	dirname, err = ioutil.TempDir("", "lineiter_test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dirname)

	err = ioutil.WriteFile(filepath.Join(dirname, "main.go"), []byte("words\n"), os.ModePerm)
	if err != nil {
		t.Fatalf("LineIter: Tried to create test file but got: %s", err)
	}

	var fileIter FileInfoIterator
	fileIter, err = NewFileIter(dirname)
	if err != nil {
		t.Fatalf("LineIter: Expected FileIter to create without error. Got: %s", err)
	}

	var lineIter LineIterator
	lineIter, err = NewMultiFileLineIterator(fileIter)
	if err != nil {
		t.Fatalf("LineIter: Expected nil but got: %s", err)
	}
	var line Line
	line, err = lineIter.Next()
	if err != nil {
		t.Fatalf("LineIter: Expected nil but got: %s", err)
	}
	if line.Text != "words" {
		t.Fatalf("Expected 'words' but got '%s'.", line)
	}
}

func TestLineIteratorMultiLine(t *testing.T) {
	var err error
	var dirname string

	dirname, err = ioutil.TempDir("", "lineiter_test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dirname)

	err = ioutil.WriteFile(filepath.Join(dirname, "main.go"), []byte("words1\nwords2\n"), os.ModePerm)
	if err != nil {
		t.Fatalf("LineIter: Tried to create test file but got: %s", err)
	}

	var fileIter FileInfoIterator
	fileIter, err = NewFileIter(dirname)
	if err != nil {
		t.Fatalf("LineIter: Expected FileIter to create without error. Got: %s", err)
	}

	var lineIter LineIterator
	lineIter, err = NewMultiFileLineIterator(fileIter)
	if err != nil {
		t.Fatalf("LineIter: Expected nil but got: %s", err)
	}
	var line Line
	line, err = lineIter.Next()
	if err != nil {
		t.Fatalf("LineIter: Expected nil but got: %s", err)
	}
	if line.Text != "words1" {
		t.Fatalf("Expected 'words1' but got '%s'.", line)
	}
	line, err = lineIter.Next()
	if err != nil {
		t.Fatalf("LineIter: Expected nil but got: %s", err)
	}
	if line.Text != "words2" {
		t.Fatalf("Expected 'words2' but got '%s'.", line)
	}
}

func TestLineIteratorFileExhaustion(t *testing.T) {
	var err error
	var dirname string

	dirname, err = ioutil.TempDir("", "lineiter_test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dirname)

	err = ioutil.WriteFile(filepath.Join(dirname, "main.go"), []byte("words\n"), os.ModePerm)
	if err != nil {
		t.Fatalf("LineIter: Tried to create test file but got: %s", err)
	}

	var fileIter FileInfoIterator
	fileIter, err = NewFileIter(dirname)
	if err != nil {
		t.Fatalf("LineIter: Expected FileIter to create without error. Got: %s", err)
	}

	var lineIter LineIterator
	lineIter, err = NewMultiFileLineIterator(fileIter)
	if err != nil {
		t.Fatalf("LineIter: Expected nil but got: %s", err)
	}

	_, err = lineIter.Next()
	if err != nil {
		t.Fatalf("LineIter: Expected nil but got: %s", err)
	}
	_, err = lineIter.Next()
	switch err.(type) {
	case IteratorEmptyError:
		break
	default:
		t.Fatalf("LineIter: Expected IteratorEmptyError but got: %s", err)
	}

}

func TestLineIteratorFileFailover(t *testing.T) {
	var err error
	var dirname string

	dirname, err = ioutil.TempDir("", "lineiter_test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dirname)

	err = ioutil.WriteFile(filepath.Join(dirname, "main.go"), []byte("words1\n"), os.ModePerm)
	if err != nil {
		t.Fatalf("LineIter: Tried to create test file but got: %s", err)
	}
	err = ioutil.WriteFile(filepath.Join(dirname, "main_test.go"), []byte("words2\n"), os.ModePerm)
	if err != nil {
		t.Fatalf("LineIter: Tried to create test file but got: %s", err)
	}

	var fileIter FileInfoIterator
	fileIter, err = NewFileIter(dirname)
	if err != nil {
		t.Fatalf("LineIter: Expected FileIter to create without error. Got: %s", err)
	}

	var lineIter LineIterator
	lineIter, err = NewMultiFileLineIterator(fileIter)
	if err != nil {
		t.Fatalf("LineIter: Expected nil but got: %s", err)
	}

	var line Line
	line, err = lineIter.Next()
	if err != nil {
		t.Fatalf("LineIter: Expected nil but got: %s", err)
	}
	if line.Text != "words1" {
		t.Fatalf("Expected 'words1' but got '%s'.", line)
	}
	line, err = lineIter.Next()
	if err != nil {
		t.Fatalf("LineIter: Expected nil but got: %s", err)
	}
	if line.Text != "words2" {
		t.Fatalf("Expected 'words2' but got '%s'.", line)
	}

}

type fixtureLineIter struct {
	lines  []Line
	errs   []error
	offset uint64
}

func (i *fixtureLineIter) Next() (Line, error) {
	var line Line
	var err error
	line, err, i.offset = i.lines[i.offset], i.errs[i.offset], i.offset+1

	return line, err
}
func TestPatternIterator(t *testing.T) {
	var err error

	var lineIter = fixtureLineIter{
		lines:  []Line{Line{Text: "boring"}, Line{Text: "TODO"}, Line{Text: "TODO(someone)"}, Line{}},
		errs:   []error{nil, nil, nil, IteratorEmptyError{}},
		offset: 0,
	}

	var pattern *regexp.Regexp
	pattern, err = regexp.Compile("TODO.*")
	if err != nil {
		t.Fatalf("PatternIter: Failed to compile regex: %s", err)
	}

	var patternIter LineIterator
	patternIter, err = NewRegexpLineIteratorWrapper(pattern, &lineIter)
	if err != nil {
		t.Fatalf("PatternIter: Faild to create the PatternIter: %s", err)
	}

	var line Line
	line, err = patternIter.Next()
	if err != nil {
		t.Fatalf("PatternIter: Expected to get a line but got: %s", err)
	}
	if line.Text != "TODO" {
		t.Fatalf("PatternIter: Expected the text TODO but got: %s", err)
	}

	line, err = patternIter.Next()
	if err != nil {
		t.Fatalf("PatternIter: Expected to get a line but got: %s", err)
	}
	if line.Text != "TODO(someone)" {
		t.Fatalf("PatternIter: Expected the text TODO(someone) but got: %s", err)
	}

	_, err = patternIter.Next()
	switch err.(type) {
	case IteratorEmptyError:
		break
	default:
		t.Fatalf("LineIter: Expected IteratorEmptyError but got: %s", err)
	}
}

func TestNegativePatternIterator(t *testing.T) {
	var err error

	var lineIter = fixtureLineIter{
		lines:  []Line{Line{Text: "boring"}, Line{Text: "TODO"}, Line{Text: "TODO(someone)"}, Line{}},
		errs:   []error{nil, nil, nil, IteratorEmptyError{}},
		offset: 0,
	}

	var pattern *regexp.Regexp
	pattern, err = regexp.Compile("TODO.*")
	if err != nil {
		t.Fatalf("NegativePatternIter: Failed to compile regex: %s", err)
	}

	var patternIter LineIterator
	patternIter, err = NewNegativeRegexpLineIteratorWrapper(pattern, &lineIter)
	if err != nil {
		t.Fatalf("NegativePatternIter: Faild to create the PatternIter: %s", err)
	}

	var line Line
	line, err = patternIter.Next()
	if err != nil {
		t.Fatalf("NegativePatternIter: Expected to get a line but got: %s", err)
	}
	if line.Text != "boring" {
		t.Fatalf("NegativePatternIter: Expected the text boring but got: %s", err)
	}

	_, err = patternIter.Next()
	switch err.(type) {
	case IteratorEmptyError:
		break
	default:
		t.Fatalf("LineIter: Expected IteratorEmptyError but got: %s", err)
	}
}
