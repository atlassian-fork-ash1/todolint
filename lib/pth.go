package todolint

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"time"
)

// PrefixedPathFileInfo is a FileInfo implementation that consumes a FileInfo
// implementation and a path prefix in order to always return the absolute
// file path for calls to Name(). All other calls are proxied to the underlying
// FileInfo implementation.
type PrefixedPathFileInfo struct {
	prefix          string
	wrappedFileInfo os.FileInfo
}

// Name fetches the prefixed path.
func (f PrefixedPathFileInfo) Name() string {
	return filepath.Join(f.prefix, f.wrappedFileInfo.Name())
}

// Size fetches the length in bytes.
func (f PrefixedPathFileInfo) Size() int64 {
	return f.wrappedFileInfo.Size()
}

// Mode fetches the associated FileMode.
func (f PrefixedPathFileInfo) Mode() os.FileMode {
	return f.wrappedFileInfo.Mode()
}

// ModTime fetches the last modified time.
func (f PrefixedPathFileInfo) ModTime() time.Time {
	return f.wrappedFileInfo.ModTime()
}

// IsDir fetches whether or not the file is a directory.
func (f PrefixedPathFileInfo) IsDir() bool {
	return f.wrappedFileInfo.IsDir()
}

// Sys fetches the underlying data source.
func (f PrefixedPathFileInfo) Sys() interface{} {
	return f.wrappedFileInfo.Sys()
}

func (f PrefixedPathFileInfo) String() string {
	return f.Name()
}

// ReadDir is an implementation of os.ReadDir that returns PrefixedPathFileInfo
// objects that have the file name prefixed with the given dirname.
func ReadDir(dirname string) ([]os.FileInfo, error) {
	var err error
	var originalResults []os.FileInfo
	var prefixedResults = make([]os.FileInfo, 0)

	originalResults, err = ioutil.ReadDir(dirname)
	if err != nil {
		return nil, err
	}

	for o := range originalResults {
		prefixedResults = append(prefixedResults, PrefixedPathFileInfo{
			prefix:          dirname,
			wrappedFileInfo: originalResults[o],
		})
	}

	return prefixedResults, nil
}
